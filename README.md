# Ramas

1. Crea la rama develop

´´´
git checkout -b develop
´´´

2. Subir rama develop

´´´
git push origin develop
´´´

3. Crear otra rama
´´´
git checkout -b feature/estructura-html develop
´´´

4. Hacer merge de los nuevos cambios
´´´
git checkout develop
git merge --no-ff feature/estructura-html
´´´

5. Quitar rama una vez hecho el merge
´´´
git branch -d feature/estructura-html
´´´

